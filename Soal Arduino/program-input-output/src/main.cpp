#include <Arduino.h>

// Relay
#define RELAY_CH01 GPIO_NUM_16
#define RELAY_CH02 GPIO_NUM_17
#define RELAY_CH03 GPIO_NUM_18
#define RELAY_CH04 GPIO_NUM_19

// Push button
#define PUSH_BUTTON1 GPIO_NUM_21
#define PUSH_BUTTON2 GPIO_NUM_22

void setup()
{
  // Setup Serialport
  Serial.begin(115200);

  // Setup OUTPUT
  Serial.println("[DBG] Setup Ouput");
  pinMode(RELAY_CH01, OUTPUT);
  pinMode(RELAY_CH02, OUTPUT);
  pinMode(RELAY_CH03, OUTPUT);
  pinMode(RELAY_CH04, OUTPUT);

  // Setup INPUT
  Serial.println("[DBG] Setup Input");
  pinMode(PUSH_BUTTON1, INPUT);
  pinMode(PUSH_BUTTON2, INPUT);

  Serial.println("[DBG] Setup Done..!");
}

/*
Silahkan tulis program didalam fungsi loop.
Jangan menambahkan fungsi yang baru.ß
 */
void loop()
{
  // Code here..
}